package com.luv2code.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.bo.BankBOImpl;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Customer;

/**
 * Servlet implementation class AddAccountToBankServlet
 */
@WebServlet("/AddAccountToBankServlet")
public class AddAccountToBankServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private BankBO bankBO;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddAccountToBankServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() throws ServletException {
		super.init();
		bankBO = new BankBOImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// Step 1: set the content type
	     response.setContentType("text/html;charset=UTF-8"); 

		// step 2: get the printWriter
		PrintWriter out = response.getWriter(); 

		// step 3: generate or create the HTML element
		AccountType accountTypeSet = null;
		String accountType = request.getParameter("accountType");

		if (accountType.equals("1")) {
			accountTypeSet = AccountType.CHECKING;
		} else if (accountType.equals("2")) {
			accountTypeSet = AccountType.SAVING;
		} else if (accountType.equals("3")) {
			accountTypeSet = AccountType.BUSINESS_CHECKING;
		} else {
			accountTypeSet = AccountType.CHECKING;
		}

		
		Account account = new Account(accountTypeSet,new Date(), Integer.parseInt(request.getParameter("amount")));

		ArrayList<Account> accountList = new ArrayList<Account>();
		accountList.add(account);
		
		// Setting the attribute of the request object
		// which will be later fetched by a JSP page
		request.setAttribute("accountList", accountList);


		// Creating a RequestDispatcher object to dispatch
		// the request the request to another resource
		RequestDispatcher rd = request.getRequestDispatcher("add-accounts-Print.jsp");

		// The request will be forwarded to the resource
		// specified, here the resource is a JSP named,
		// "stdlist.jsp"
		rd.forward(request, response);


		HttpSession session = request.getSession();
		String ssn = (String) session.getAttribute("ssn");

		Customer customer = bankBO.getCustomerInfo(ssn);
		System.out.println(customer);

		bankBO.openAccount(customer, account);
		

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
