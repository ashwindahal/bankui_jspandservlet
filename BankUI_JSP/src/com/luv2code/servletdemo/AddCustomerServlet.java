package com.luv2code.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.bo.BankBOImpl;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

/**
 * Servlet implementation class BanksServlet
 */
@WebServlet("/AddCustomerServlet")
public class AddCustomerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private BankBO bankBO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCustomerServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	bankBO = new BankBOImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Step 1: set the content type
	     response.setContentType("text/html;charset=UTF-8"); 
				
				
		//step 2: get the printWriter
		PrintWriter out = response.getWriter();
		
		//step 3: generate or create the HTML element 
		if(request.getParameter("firstName").isEmpty()
				||request.getParameter("lastName").isEmpty()
				||request.getParameter("ssn").isEmpty()
				||request.getParameter("addressLine").isEmpty()
				||request.getParameter("city").isEmpty()
				||request.getParameter("zipCode").isEmpty()
				||request.getParameter("stateCode").isEmpty()) {
			
			RequestDispatcher rd = request.getRequestDispatcher("add-customer.html");
			out.println("<font color=red><Strong>Please Complete all fileds</Strong></font>");
			rd.include(request, response);
		}
		else {
			
			Customer customer = new Customer();
			customer.setFirstName( request.getParameter("firstName") );
			customer.setLastName( request.getParameter("lastName") );
			customer.setSsn(request.getParameter("ssn"));
			
			Address address = new Address(null, null, null, null);
			address.setAddressLine(request.getParameter("addressLine"));
			address.setCity(request.getParameter("city"));
			address.setZipCode(request.getParameter("zipCode"));
			address.setStateCode(request.getParameter("stateCode"));
			
			
			HttpSession session = request.getSession();
			session.setAttribute("ssn", customer.getSsn());
	
			customer.setAddress(address);
			bankBO.addCustomer(customer);
			
			
			ArrayList<Customer> customerList = new ArrayList<Customer>();
			customerList.add(customer); 
			
			// Setting the attribute of the request object 
		    // which will be later fetched by a JSP page 
		    request.setAttribute("customerList", customerList); 
			
		 // Creating a RequestDispatcher object to dispatch 
		       // the request the request to another resource 
		         RequestDispatcher rd =  
		             request.getRequestDispatcher("add-customer-Print.jsp"); 
		  
		       // The request will be forwarded to the resource  
		       // specified, here the resource is a JSP named, 
		       // "stdlist.jsp" 
		          rd.forward(request, response); 
 			
		}		
		
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		
	}

}
