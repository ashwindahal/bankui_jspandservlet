package com.luv2code.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.bo.BankBOImpl;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;

/**
 * Servlet implementation class AddAccountServlet
 */
@WebServlet("/AddAccountServlet")
public class AddAccountServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
	private BankBO bankBO;
	private Bank bank; 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAccountServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	bankBO = new BankBOImpl();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//Step 1: set the content type
		response.setContentType("text/html");
				
				
		//step 2: get the printWriter
		PrintWriter out = response.getWriter();
		
		Customer customer = bankBO.getCustomerInfo(request.getParameter("ssn")); 
		
		
		if(request.getParameter("ssn").isEmpty())
		{
			RequestDispatcher rd = request.getRequestDispatcher("add-account.html");
			out.println("<font color=red><Strong>Please Provide Customer SSN</Strong></font>");
			rd.include(request, response);
		}
		else if(customer == null) {
			RequestDispatcher rd = request.getRequestDispatcher("customerErrorPage.html");
			rd.forward(request, response);	
		}
		else
		{
			RequestDispatcher rd = request.getRequestDispatcher("add-account-to-bank.html");
			rd.forward(request, response);
		}
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
