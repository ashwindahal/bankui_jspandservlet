package com.cerotid.web.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

public class CustomerDbUtil {
	
	private DataSource dataSource;
	
	public CustomerDbUtil(DataSource theDataSource) {
		dataSource = theDataSource; 
		
	}
	
	

	public List<Customer> getCustomers() throws Exception {
		
		//creating a list of customers 
		List<Customer> customers = new ArrayList<>();
		
		//creating the database objects 
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null; 
		
		try {
			//get Connection 
			myConn = dataSource.getConnection();
			
			//create sql statement
			String sql = "select * from customer";
			
			myStmt = myConn.createStatement();
			
			//execute the query 
			myRs = myStmt.executeQuery(sql);
			
			//process the result set 
			
			while(myRs.next()) {
				//customer fields
				String firstName = myRs.getString("firstName");
				String lastName = myRs.getString("lastName");
				String ssn = myRs.getString("ssn");
				//address object
				Object cusAddress = myRs.getObject("address"); 
				Address address = (Address) cusAddress; 
				
				//create new customer object 
				Customer tempCustomer = new Customer(firstName,lastName,ssn,address); 
				
				//add it to the list of customers
				customers.add(tempCustomer);
				
			}
			
			return customers; 
			
			
			
		}
		finally {
			
			//close jdbc objects
			close(myConn,myStmt,myRs);
		}
			
		
	}



	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {
		try {
			if(myConn != null) {
				myConn.close();
			}
			if(myStmt != null) {
				myStmt.close();
			}
			if(myRs != null) {
				myRs.close();
			}
		}
		catch(Exception exe) {
			exe.printStackTrace();
		}
	}

}
