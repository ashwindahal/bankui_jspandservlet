<%@page import="com.cerotid.bank.model.*"%> 
<%@page import="java.util.*"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html> 
<html> 
<head>
	<title>	
		 Add Customer
	</title>
	<link rel="stylesheet" type="text/css" href="page-style.css"/>
</head>
<body >
<a href="http://www.cerotid.com" Title="Link to Cerotid Website"> <img src="logo.png" alt="logo" class="center" /></a>
<hr/>
<body> 
	<h2>Displaying Customer List</h2> 
	<table border ="2" width="700" align="center"> 
		<tr bgcolor="#ff6600"> 
		<th><b>First Name</b></th>  
		<th><b>Last Name</b></th>  
		<th><b>SSN</b></th>  
		<th><b>Address</b></th>  
		</tr> 
        <%-- Fetching the attributes of the request object 
             which was previously set by the servlet  
              "AddCustomerServlet.java" 
        --%>  
        <%ArrayList<Customer> customerList =(ArrayList<Customer>)request.getAttribute("customerList"); 
        for(Customer c:customerList){%> 
        <%-- Arranging data in tabular form 
        --%> 
            <tr> 
                <td><%=c.getFirstName()%></td>  
                <td><%=c.getLastName()%></td>    
                 <td><%=c.getSsn()%></td> 
                  <td><%=c.getAddress()%></td> 
            </tr> 
            <%}%> 
		</table> 
		<a href ="add-customer.html" Title="Link to Add Customer"><h2>Add more Customers</h2> </a>
		<a href ="homepage.html" Title="Return to Homepage"> <h2>Return to Homepage</h2></a>
		<br/><br/>
		
	</body> 
</html> 
