<%@page import="com.cerotid.bank.model.*"%> 
<%@page import="java.util.*"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<!DOCTYPE html> 
<html> 
<head>
	<title>	
		 Add Customer
	</title>
	<link rel="stylesheet" type="text/css" href="page-style.css"/>
</head>
<body >
<a href="http://www.cerotid.com" Title="Link to Cerotid Website"> <img src="logo.png" alt="logo" class="center" /></a>
<hr/>
<body> 
	<h2>Displaying Customer Account</h2> 
	<table border ="2" width="700" align="center"> 
		<tr bgcolor="#ff6600"> 
		<th><b>Account Type</b></th>  
		<th><b>Opening Date</b></th>  
		<th><b>Account Balance</b></th>  
	
		</tr> 
        <%-- Fetching the attributes of the request object 
             which was previously set by the servlet  
              "AddCustomerServlet.java" 
        --%>  
        <%ArrayList<Account> accountList =(ArrayList<Account>)request.getAttribute("accountList"); 
        for(Account a:accountList){%> 
        <%-- Arranging data in tabular form 
        --%> 
            <tr> 
                <td><%=a.getAccountType()%></td>  
                <td><%=a.getAccountOpenDate()%></td>    
                 <td><%=a.getAmount()%></td> 
            </tr> 
            <%}%> 
		</table> 
		<a href ="add-account.html" Title="Link to Add Customer"><h2>Add another account</h2> </a>
		<a href ="homepage.html" Title="Return to Homepage"> <h2>Return to Homepage</h2></a>
		<br/><br/>
		
	</body> 
</html> 
