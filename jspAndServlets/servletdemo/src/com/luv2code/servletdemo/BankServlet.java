package com.luv2code.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.bo.BankBOImpl;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

/**
 * Servlet implementation class BanksServlet
 */
@WebServlet("/BankServlet")
public class BankServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private BankBO bankBO;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BankServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    @Override
    public void init() throws ServletException {
    	// TODO Auto-generated method stub
    	super.init();
    	bankBO = new BankBOImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Step 1: set the content type
		response.setContentType("text/html");
				
				
		//step 2: get the printWriter
		PrintWriter out = response.getWriter();
		
		//step 3: generate or create the HTML element 
		out.println("<html><body>");
		
		out.println("<strong>Customers</strong>");
		
		out.println(); 
		out.println(); 
		
		out.println("Customer " 
				+ request.getParameter("firstName") 
				+ " " + request.getParameter("lastName")
				+ " " + " was added"); 
		
		Customer customer = new Customer();
		customer.setFirstName( request.getParameter("firstName") );
		customer.setLastName( request.getParameter("lastName") );
		customer.setSsn(request.getParameter("fieldSsn"));
		
		Address address = new Address(null, null, null, null);
		address.setAddressLine(request.getParameter("addressLine"));
		address.setCity(request.getParameter("city"));
		address.setZipCode(request.getParameter("zipCode"));
		address.setStateCode(request.getParameter("stateCode"));
		
		customer.setAddress(address);

		bankBO.addCustomer(customer);
		
		out.println(customer); 
		
		out.println("</body></html>"); 
		
		
	}
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		
	}

}
